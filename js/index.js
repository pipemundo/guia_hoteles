var botonAbreModal = null;
var claseOriginal = "btn-reserva";
var claseDeshabilitar = "btn-reserva-deshabilitado";

$(document).ready(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    $('.carousel').carousel({
        interval: 2000
    });

    /*Capturar en de acuerdo al botón (botonAbreModal) de reserva que haya disparado con click el abrir modal para posteriormente cambiar su apariencia*/
    $('[data-target="#modalContacto"]').on('click', function() {
        /*Capturar el boton que ha disparado el modal*/
        botonAbreModal = this;
        /*Obtener el nombre del hotel de acuerdo a la ubicación del botón de reserva, para mostarrlo en el modal*/
        var nombreHotel = $(botonAbreModal).parent().parent('.card-hotel').find('h4.card-title').html();
        $("#idHotelContacto").html(nombreHotel);

    });

    $('#modalContacto').on('show.bs.modal', function(e) {

        console.log('1. cuando comienza a abrirse');
        $(botonAbreModal).attr('disabled', true);
        $(botonAbreModal).removeClass(claseOriginal).addClass(claseDeshabilitar);

    });

    $('#modalContacto').on('shown.bs.modal', function(e) {
        console.log('2. cuando se terminó de abrir');
    });

    $('#modalContacto').on('hide.bs.modal', function(e) {
        console.log('3. cuando comienza a ocultarse');

    });
    $('#modalContacto').on('hidden.bs.modal', function(e) {

        console.log('4. cuando se terminó de ocultar');
        $(botonAbreModal).attr('disabled', false);
        $(botonAbreModal).removeClass(claseDeshabilitar).addClass(claseOriginal);

    });

    $('#modalContacto #btnEnviarModal').on('click', function() {
        $('#modalContacto').modal('hide');
    });


});